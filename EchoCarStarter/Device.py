#!/usr/bin/env python
""" fauxmo_minimal.py - Fabricate.IO
    This is a demo python file showing what can be done with the debounc$
    The handler prints True when you say "Alexa, device on" and False wh$
    "Alexa, device off".
    If you have two or more Echos, it only handles the one that hears yo$
    You can have an Echo per room and not worry about your handlers trig$
    those other rooms.
    The IP of the triggering Echo is also passed into the act() function$
    do different things based on which Echo triggered the handler.
"""

import fauxmo
import logging
import time  ##import or include?
import RPi.GPIO as GPIO


from debounce_handler import debounce_handler

logging.basicConfig(level=logging.DEBUG)

Const int REMOTE_GPIO = 9  ##does this need definition

class device_handler(debounce_handler):
    """Publishes the on/off state requested,
       and the IP address of the Echo making the request.
    """
    TRIGGERS = {"The Ex": 52000}

    def act(self, client_address, state):
        print "State", state, "from client @", client_address
        changeIOState(state)
        return True

def setupIO():
    GPIO.cleanup()
    GPIO.setmode(GPIO.BCM)  ##Chip Number 9 Not Board pin number 9
    GPIO.setup(REMOTE_GPIO,GPIO.OUT)
    GPIO.output(REMOTE_GPIO,GPIO.LOW) #make sure the car is off

def changeIOState(state): ##simulate long key press
    if state:
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)
        time.sleep(.200)

    else:
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.HIGH)
        time.sleep(.200)
        GPIO.output(REMOTE_GPIO,GPIO.LOW)



if __name__ == "__main__":
    # Startup the fauxmo server
    fauxmo.DEBUG = True
    setupIO()
    p = fauxmo.poller()
    u = fauxmo.upnp_broadcast_responder()
    u.init_socket()
    p.add(u)

    # Register the device callback as a fauxmo handler
    d = device_handler()
    for trig, port in d.TRIGGERS.items():
        fauxmo.fauxmo(trig, u, p, None, port, d)

    # Loop and poll for incoming Echo requests
    logging.debug("Entering fauxmo polling loop")
    while True:
        try:
            # Allow time for a ctrl-c to stop the process
            p.poll(100)
            time.sleep(0.1)
        except Exception, e:
            logging.critical("Critical exception: " + str(e))
            break
            